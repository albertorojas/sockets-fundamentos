var socket = io()

//Escucha sucesos
socket.on('connect', function() {
    console.log('conectado con el servidor...')
})

socket.on('disconnect', function() {
    console.log('Perdimos conexión con el servidor...')
})

//Envia informacion
socket.emit('enviarMensaje', {
    usuario: 'Alberto Rojas',
    mensaje: 'Hola Mundo...'
}, function(res) {
    console.log('Respuesta Server: ', res)
})


//Escuchar informacion
socket.on('enviarMensaje', function(mensaje) {
    console.log('Servidor: ', mensaje)
})